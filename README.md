# Rabo Bank Service
project desc

## Release notes:
### 0.0.1-SNAPSHOT
- User can upload csv or xml file for validating Customer Statement.

## API Endpoint 
The below end point is used to upload the file.

we need to setup a location to save the uploading file through <b>document.repository.service.local.upload.dir</b> property file in
application.property file. 

<br/>
<b>Http POST method</b>
<br/>
http://localhost:8082/rabobank/api/v1/file-read 
	
	Param 
	[
		{
			"key":"uploadingFiles",
			"description":"",
			"type":"file",
			"enabled":true,
			"value":[]
		}
	]
####Response
where we will get response of empty result with http 200 response  if all data in a file is validated successfully.
<br/>If any data is failed in while doing validation will be response with its reference, description and short description about the failure,
the Http response will be 400.






