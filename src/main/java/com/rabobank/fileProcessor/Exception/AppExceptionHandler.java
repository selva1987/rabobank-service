package com.rabobank.fileProcessor.Exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * This method used to catch the Exception when the user upload a file which is not in csv or xml format.
     * @param e throwed exception.
     * @param request
     * @return
     */
    @ExceptionHandler({FileTypeNotSupportException.class})
    protected ResponseEntity<Object> handleFileTypeNotSupportException(FileTypeNotSupportException e, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(e, e.getMessage(), headers, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * This method is used to catch the exception when the data in a file is not passed through validation check.
     * @param e
     * @param request
     * @return
     */
    @ExceptionHandler(DataValidationException.class)
    protected ResponseEntity<Object> handleDataValidationException(DataValidationException e, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return handleExceptionInternal(e, e.getTransactionList(), headers, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * This method is used to catch the exception when unknown exception like sending large file size, this exception will be handled.
     * @param exception
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleCommonException(Exception exception) {
        String error = exception.getMessage();
        String message = "An error occurred while serving your request!";
        logger.error(message, exception);
        ResponseEntity<Object> response = new ResponseEntity<>(error,
                HttpStatus.INTERNAL_SERVER_ERROR);
        return response;
    }
}
