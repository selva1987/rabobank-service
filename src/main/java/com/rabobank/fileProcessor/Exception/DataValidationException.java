package com.rabobank.fileProcessor.Exception;

import com.rabobank.fileProcessor.dto.DocumentUploadExceptionDto;

import java.util.List;

public class DataValidationException extends Exception {

    private static final long serialVersionUID = 1L;
    private List<DocumentUploadExceptionDto> transactionList;

    public DataValidationException(List<DocumentUploadExceptionDto> transactionList, String message) {
        super(message);
        this.transactionList = transactionList;
    }

    public List<DocumentUploadExceptionDto> getTransactionList() {
        return transactionList;
    }
}
