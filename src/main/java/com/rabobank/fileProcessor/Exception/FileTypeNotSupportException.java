package com.rabobank.fileProcessor.Exception;

public class FileTypeNotSupportException extends Exception {

    public FileTypeNotSupportException(String message) {
        super(message);
    }
}
