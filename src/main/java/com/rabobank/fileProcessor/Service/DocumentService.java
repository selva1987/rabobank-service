package com.rabobank.fileProcessor.Service;

import com.rabobank.fileProcessor.dto.DocumentUploadResponseDto;
import org.springframework.web.multipart.MultipartFile;

public interface DocumentService {
    DocumentUploadResponseDto uploadDocument(MultipartFile uploadingFiles) throws Exception;
}
