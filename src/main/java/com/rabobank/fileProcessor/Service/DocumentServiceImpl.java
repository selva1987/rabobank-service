package com.rabobank.fileProcessor.Service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.rabobank.fileProcessor.Exception.DataValidationException;
import com.rabobank.fileProcessor.Exception.FileTypeNotSupportException;
import com.rabobank.fileProcessor.dto.DocumentUploadExceptionDto;
import com.rabobank.fileProcessor.dto.Transaction;
import com.rabobank.fileProcessor.dto.XmlTransaction;
import com.rabobank.fileProcessor.dto.DocumentUploadResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DocumentServiceImpl implements DocumentService{

    @Value("${document.repository.service.local.upload.dir}")
    public String tempUploadingDir;

    /**
     * Uploaded file will be validated and response back to user.
     * @param uploadingFiles user uploaded file.
     * @return empty object as success.
     * @throws FileTypeNotSupportException
     * @throws JAXBException
     * @throws IOException
     * @throws DataValidationException
     */
    @Override
    public DocumentUploadResponseDto uploadDocument(MultipartFile uploadingFiles) throws FileTypeNotSupportException, JAXBException, IOException, DataValidationException {
        String fileExtension = getFileExtension(uploadingFiles.getOriginalFilename());
        List<DocumentUploadExceptionDto> documentUploadExceptionList = new ArrayList<>();
        if(fileExtension.equalsIgnoreCase("csv")) {
            return readCsvFile(uploadingFiles);
        } else if(fileExtension.equalsIgnoreCase("xml")) {
            return readXmlFile(uploadingFiles);
        } else {
            throw new FileTypeNotSupportException("Please Upload csv or xml formatted file.");
        }
    }

    /**
     * The method is used to read csv formatted file and do an validation.
     * @param uploadingFiles user uploaded file
     * @return empty object as success as there is no validation fail occur.
     * @throws Exception
     */
    private DocumentUploadResponseDto readCsvFile(MultipartFile uploadingFiles) throws DataValidationException, IOException {
        List<DocumentUploadExceptionDto> documentUploadExceptionList = new ArrayList<>();
        CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
        CsvMapper mapper = new CsvMapper();
        File file = new File(tempUploadingDir + uploadingFiles.getOriginalFilename());
        uploadingFiles.transferTo(file);
        MappingIterator<Transaction> readValues =
                mapper.readerWithSchemaFor(Transaction.class).with(bootstrapSchema).readValues(file);
        List<Transaction> transactionList =  readValues.readAll();
        validateTransactionFile(transactionList);
        return new DocumentUploadResponseDto();
    }

    /**
     * Read xml formatted file for validation process.
     * @param uploadingFiles user uploaded file
     * @return empty object as success as there is no validation fail occur.
     * @throws IOException
     * @throws JAXBException
     * @throws DataValidationException
     */
    private DocumentUploadResponseDto readXmlFile(MultipartFile uploadingFiles) throws IOException, JAXBException, DataValidationException {
        File file = new File(tempUploadingDir + uploadingFiles.getOriginalFilename());
        uploadingFiles.transferTo(file);
        JAXBContext jaxbContext = JAXBContext.newInstance(XmlTransaction.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        XmlTransaction transaction = (XmlTransaction) jaxbUnmarshaller.unmarshal(file);
        List<Transaction> transactionList = transaction.getRecord().stream().map(obj-> new Transaction(obj.getReference(),obj.getAccountNumber(),obj.getDescription(),obj.getStartBalance
                (),obj.getMutation(), obj.getEndBalance())).collect(Collectors.toList());
        validateTransactionFile(transactionList);
        return new DocumentUploadResponseDto();
    }

    /**
     * Validation will be process for the transaction in file
     * @param transactionList the records in file
     * @throws DataValidationException
     */
    private void validateTransactionFile(List<Transaction> transactionList) throws DataValidationException {
        List<DocumentUploadExceptionDto> validationResultList = transactionList.stream().filter(i -> Collections.frequency(transactionList, i) >1)
                .map(obj -> new DocumentUploadExceptionDto(obj.getReference(), obj.getDescription(),String.format("Reference is %s is duplicate", obj.getReference())))
                .collect(Collectors.toList());
        validationResultList.addAll(transactionList.stream().filter(TransactionPredicate.isEndBalanceValid())
                .map(obj -> new DocumentUploadExceptionDto(obj.getReference(), obj.getDescription(),"End Balance didnt have correct value "))
                .collect(Collectors.toList()));

        if(!validationResultList.isEmpty()){
            throw new DataValidationException(validationResultList, "Exception while processing data");
        }
    }

    /**
     * To get the file extension to check whether the file is csv or xml or in any other not supported format.
     * @param fileName user uploaded file name with extension.
     * @return file extension.
     */
    private String getFileExtension(final String fileName) {
        if (fileName.lastIndexOf('.') != -1 && fileName.lastIndexOf('.') != 0) {
            return fileName.substring(fileName.lastIndexOf('.') + 1);
        } else {
            return "";
        }
    }
}

