package com.rabobank.fileProcessor.Service;

import com.rabobank.fileProcessor.dto.Transaction;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Predicate;

@Service
public class TransactionPredicate {

    public static Predicate<Transaction> isEndBalanceValid() {
        return p -> {
            Double endBalance = p.getStartBalance() + p.getMutation();
            endBalance = new BigDecimal(endBalance).setScale(2, RoundingMode.HALF_UP).doubleValue();
            if(!endBalance.equals(p.getEndBalance())){
                return true;
            }
            return false;
        };
    }
}
