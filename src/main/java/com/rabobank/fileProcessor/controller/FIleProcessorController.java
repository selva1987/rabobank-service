package com.rabobank.fileProcessor.controller;

import com.rabobank.fileProcessor.Service.DocumentService;
import com.rabobank.fileProcessor.dto.DocumentUploadResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController("/api/v1/file-read")
public class FIleProcessorController {

    private final DocumentService documentService;

    public FIleProcessorController(DocumentService documentService) {
        this.documentService = documentService;
    }

    /**
     * The API endpoint to upload file for validation check.
     * @param uploadingFiles user uploaded file.
     * @return empty object with 200 response.
     * @throws Exception
     */
    @PostMapping
    public ResponseEntity<DocumentUploadResponseDto> create(@RequestParam("uploadingFiles") MultipartFile uploadingFiles) throws Exception {
        ResponseEntity<DocumentUploadResponseDto> responseEntity = new ResponseEntity<DocumentUploadResponseDto>(
                documentService.uploadDocument(uploadingFiles), HttpStatus.OK);
        return responseEntity;
    }
}
