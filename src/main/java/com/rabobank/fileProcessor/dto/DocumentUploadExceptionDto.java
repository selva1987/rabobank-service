package com.rabobank.fileProcessor.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DocumentUploadExceptionDto {
    private Integer transactionReference;
    private String description;
    private String message;

    public DocumentUploadExceptionDto(Integer transactionReference, String description, String message) {
        this.transactionReference = transactionReference;
        this.description = description;
        this.message = message;

    }
}
