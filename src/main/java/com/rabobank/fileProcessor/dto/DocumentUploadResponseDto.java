package com.rabobank.fileProcessor.dto;

import lombok.Data;

import java.util.List;

@Data
public class DocumentUploadResponseDto {
    private List<DocumentUploadExceptionDto> documentUploadExceptionList;

}
