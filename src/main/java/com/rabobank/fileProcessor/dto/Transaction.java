package com.rabobank.fileProcessor.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class Transaction {

    @JsonProperty("Reference")
    private Integer reference;
    @JsonProperty("AccountNumber")
    private String accountNumber;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("Start Balance")
    private Double startBalance;
    @JsonProperty("Mutation")
    private Double mutation;
    @JsonProperty("End Balance")
    private Double endBalance;

    public Transaction(Integer reference, String accountNumber, String description, Double startBalance, Double mutation, Double endBalance) {
        this.reference = reference;
        this.accountNumber = accountNumber;
        this.description = description;
        this.startBalance = startBalance;
        this.mutation = mutation;
        this.endBalance = endBalance;
    }

    @Override
    public boolean equals(Object o)
    {
        Transaction s;
        if(!(o instanceof Transaction))
        {
            return false;
        }

        else
        {
            s=(Transaction)o;
            if(this.reference.equals(s.getReference()))
            {
                return true;
            }
        }
        return false;
    }
}

