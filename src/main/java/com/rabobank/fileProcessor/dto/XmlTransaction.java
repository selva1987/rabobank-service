package com.rabobank.fileProcessor.dto;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name = "records")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class XmlTransaction {
    private List<TransactionReferenceDto> record;

}
